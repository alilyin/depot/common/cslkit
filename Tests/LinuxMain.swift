import XCTest

import CSLKitTests

var tests = [XCTestCaseEntry]()
tests += CSLKitTests.allTests()
XCTMain(tests)
