import XCTest
@testable import CSLKit

final class CSLKitTests: XCTestCase {
    
    private var csl: ClientSideLogging!
    private var journalFile: URL = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask)[0].appendingPathComponent("csl.log")

    override func setUpWithError() throws {
        CSLKit.config.verboseOutput = true
        self.csl = CSLKit.shared
        try? FileManager.default.removeItem(at: journalFile)
    }
    
    override func tearDownWithError() throws {
        self.csl = nil
        try? FileManager.default.removeItem(at: journalFile)
    }
    
    func testCSLError() {
        CSLKit.config.minSeverity = .error
        let expectation: XCTestExpectation = XCTestExpectation()
        self.csl.log("Test trace", severity: .trace)
        self.csl.log("Test debug", severity: .debug)
        self.csl.log("Test essnl", severity: .essnl)
        self.csl.log("Test warng", severity: .warng)
        self.csl.log("Test error", severity: .error)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.125) {
            XCTAssert(self.csl.currentSession.contains("Test trace") == false)
            XCTAssert(self.csl.currentSession.contains("Test debug") == false)
            XCTAssert(self.csl.currentSession.contains("Test essnl") == false)
            XCTAssert(self.csl.currentSession.contains("Test warng") == false)
            XCTAssert(self.csl.currentSession.contains("Test error") == true)
            expectation.fulfill()
        }
        self.wait(for: [expectation], timeout: 0.250)
    }

    func testCSLWarning() {
        CSLKit.config.minSeverity = .warng
        let expectation: XCTestExpectation = XCTestExpectation()
        self.csl.log("Test trace", severity: .trace)
        self.csl.log("Test debug", severity: .debug)
        self.csl.log("Test essnl", severity: .essnl)
        self.csl.log("Test warng", severity: .warng)
        self.csl.log("Test error", severity: .error)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.125) {
            XCTAssert(self.csl.currentSession.contains("Test trace") == false)
            XCTAssert(self.csl.currentSession.contains("Test debug") == false)
            XCTAssert(self.csl.currentSession.contains("Test essnl") == false)
            XCTAssert(self.csl.currentSession.contains("Test warng") == true)
            XCTAssert(self.csl.currentSession.contains("Test error") == true)
            expectation.fulfill()
        }
        self.wait(for: [expectation], timeout: 0.250)
    }
    
    func testCSLEssential() {
        CSLKit.config.minSeverity = .essnl
        let expectation: XCTestExpectation = XCTestExpectation()
        self.csl.log("Test trace", severity: .trace)
        self.csl.log("Test debug", severity: .debug)
        self.csl.log("Test essnl", severity: .essnl)
        self.csl.log("Test warng", severity: .warng)
        self.csl.log("Test error", severity: .error)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.125) {
            XCTAssert(self.csl.currentSession.contains("Test trace") == false)
            XCTAssert(self.csl.currentSession.contains("Test debug") == false)
            XCTAssert(self.csl.currentSession.contains("Test essnl") == true)
            XCTAssert(self.csl.currentSession.contains("Test warng") == true)
            XCTAssert(self.csl.currentSession.contains("Test error") == true)
            expectation.fulfill()
        }
        self.wait(for: [expectation], timeout: 0.250)
    }

    func testCSLDebug() {
        CSLKit.config.minSeverity = .debug
        let expectation: XCTestExpectation = XCTestExpectation()
        self.csl.log("Test trace", severity: .trace)
        self.csl.log("Test debug", severity: .debug)
        self.csl.log("Test essnl", severity: .essnl)
        self.csl.log("Test warng", severity: .warng)
        self.csl.log("Test error", severity: .error)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.125) {
            XCTAssert(self.csl.currentSession.contains("Test trace") == false)
            XCTAssert(self.csl.currentSession.contains("Test debug") == true)
            XCTAssert(self.csl.currentSession.contains("Test essnl") == true)
            XCTAssert(self.csl.currentSession.contains("Test warng") == true)
            XCTAssert(self.csl.currentSession.contains("Test error") == true)
            expectation.fulfill()
        }
        self.wait(for: [expectation], timeout: 0.250)
    }
      
    func testCSLTrace() {
        CSLKit.config.minSeverity = .trace
        let expectation: XCTestExpectation = XCTestExpectation()
        self.csl.log("Test trace", severity: .trace)
        self.csl.log("Test debug", severity: .debug)
        self.csl.log("Test essnl", severity: .essnl)
        self.csl.log("Test warng", severity: .warng)
        self.csl.log("Test error", severity: .error)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.125) {
            XCTAssert(self.csl.currentSession.contains("Test trace") == true)
            XCTAssert(self.csl.currentSession.contains("Test debug") == true)
            XCTAssert(self.csl.currentSession.contains("Test essnl") == true)
            XCTAssert(self.csl.currentSession.contains("Test warng") == true)
            XCTAssert(self.csl.currentSession.contains("Test error") == true)
            expectation.fulfill()
        }
        self.wait(for: [expectation], timeout: 0.250)
    }

    static var allTests = [
        ("testCSLError", testCSLError),
        ("testCSLWarning", testCSLWarning),
        ("testCSLEssential", testCSLEssential),
        ("testCSLDebug", testCSLDebug),
        ("testCSLTrace", testCSLTrace),
    ]
}
