//
//  CSL.swift
//  CSLKit
//
//  Created by alilyin on 11/8/20.
//

import Foundation

/// Client side logging engine protocol
public protocol ClientSideLogging: class {
    
    /// Logs collected during previous session
    var previousSession: String {get}
    
    /// Logs collected so far
    var currentSession: String {get}
    
    /// Creates journal item asyncronously with appropriate message and severity.
    /// - parameter msg: Log *String* message
    /// - parameter severity: Message severity level
    /// - parameter file: File name log message occurred in
    /// - parameter line: File line log message occurred in
    /// - returns: None
    func log(_ msg: String, severity: CSLSeverity, file: String, line: Int)
}

public extension ClientSideLogging {
    
    /// Creates journal item asyncronously with appropriate message and severity.
    /// - parameter msg: Log *String* message
    /// - parameter severity: Message severity level
    /// - parameter file: File name log message occurred in
    /// - parameter line: File line log message occurred in
    /// - returns: None
    func log(_ msg: String, severity: CSLSeverity, file: String = #file, line: Int = #line) {
        self.log(msg, severity: severity, file: URL(fileURLWithPath: file).lastPathComponent, line: line)
    }
}
