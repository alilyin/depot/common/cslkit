//
//  CSLSeverity.swift
//  CSLKit
//
//  Created by alilyin on 11/8/20.
//

import Foundation

/// Log message severity
public enum CSLSeverity: Int {
    
    /// Trace - Lowest, usually used for debugging
    case trace
    
    /// Debug - Low, usually used for debugging
    case debug
    
    /// Essential - Moderate
    case essnl
    
    /// Warining - high
    case warng
    
    /// Error - critical
    case error
    
    // MARK: Internal
    
    internal var description: String {
        switch self {
        case .trace: return "[TRACE]"
        case .debug: return "[DEBUG]"
        case .essnl: return "[ESSNL]"
        case .warng: return "[WARNG]"
        case .error: return "[ERROR]"
        }
    }
}
