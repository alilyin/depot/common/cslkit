//
//  CSLDateFormatter.swift
//  CSLKit
//
//  Created by alilyin on 11/16/20.
//

import Foundation

internal final class CSLDataFormatter: DateFormatter {

    override public func string(from date: Date) -> String {
        locale = Locale(identifier: "en_US_POSIX")
        timeZone = TimeZone(secondsFromGMT: 0)
        dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let components = calendar.dateComponents(Set([Calendar.Component.nanosecond]), from: date)
        let nanosecondsInMicrosecond = Double(1000)
        let microseconds = lrint(Double(components.nanosecond!) / nanosecondsInMicrosecond)
        // Subtract nanoseconds from date to ensure string(from: Date) doesn't attempt faulty rounding.
        let updatedDate = calendar.date(byAdding: .nanosecond, value: -(components.nanosecond!), to: date)!
        let dateTimeString = super.string(from: updatedDate)
        let string = String(format: "%@.%06ldZ", dateTimeString, microseconds)
        return string
    }
    
    private let microsecondsPrefix = "."
}
