//
//  CSLConfig.swift
//  CSLKit
//
//  Created by alilyin on 11/8/20.
//

import Foundation

/// CSLKit configuration
public struct CSLConfig {
    
    /// If enabled, log message will be printed to the xCode log output
    /// - warning: Default value is **false** (disabled)
    public var verboseOutput: Bool = false
    
    /// Minimum log message severity level to be added to the journal. All messages with lowest severity level will be ignored.
    /// - warning: Default value is **.essnl** (essential severity level)
    public var minSeverity: CSLSeverity = .essnl
    
    // MARK: Internal
    
    internal init() {
        
    }
}
