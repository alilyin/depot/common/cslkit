//
//  CSLKit.swift
//  CSLKit
//
//  Created by alilyin on 11/8/20.
//

import UIKit
import Foundation

/// Client side logging main class
public final class CSLKit {
    
    /// CSLKit configuration
    public static var config: CSLConfig = CSLConfig()
    
    public static let shared: ClientSideLogging = CSLKit("csl.log")
    
    /// Logs collected during previous session. Use it to attach the logs from previous session to the crash report.
    public private(set) var previousSession: String = ""
    
    /// Logs collected so far. Use it for unit testing if needed.
    public var currentSession: String {
        var result: String = ""
        self.ioQueue.sync {
            result = (try? String(contentsOf: self.journalFile)) ?? ""
        }
        return result
    }
    
    // MARK: Private
    
    private init(_ file: String) {
        self.dateFormater = CSLDataFormatter()
        self.journalFile = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask)[0].appendingPathComponent(file)
        self.ioQueue = DispatchQueue(label: "com.alilyin.cslkit", qos: DispatchQoS.background)
        self.previousSession = (try? String(contentsOf: self.journalFile)) ?? ""
        try? FileManager.default.removeItem(at: self.journalFile)
        try? self.journalFile.appendLine("==========================================================================")
        try? self.journalFile.appendLine("📱 \(UIDevice.current.detailedModel), iOS\(UIDevice.current.systemVersion)")
        try? self.journalFile.appendLine("🔋 \(UIDevice.current.batteryLevel * 100)%")
        try? self.journalFile.appendLine("==========================================================================")
    }
    
    private var ioQueue: DispatchQueue
    private var dateFormater: DateFormatter
    private var journalFile: URL
}

extension CSLKit: ClientSideLogging {
    
    public func log(_ msg: String, severity: CSLSeverity, file: String, line: Int) {
        guard severity.rawValue >= CSLKit.config.minSeverity.rawValue else { return }
        let log: String = "\(self.dateFormater.string(from: Date())) [CSL]\(severity.description): \(msg) - [\(file): \(line)]"
        if CSLKit.config.verboseOutput == true { print(log) }
        self.ioQueue.async { [weak self] in
            try? self?.journalFile.appendLine(log)
        }
    }
}

private extension URL {
    func appendLine(_ text: String) throws {
        try? (text + "\n").data(using: .utf8)?.appendTo(file: self)
    }
}

private extension Data {
    func appendTo(file url: URL) throws {
        if let fileHandle = FileHandle(forWritingAtPath: url.path) {
            defer {
                fileHandle.closeFile()
            }
            fileHandle.seekToEndOfFile()
            fileHandle.write(self)
        } else {
            try write(to: url, options: .atomic)
        }
    }
}

private extension UIDevice {
    
    var detailedModel: String {
        var sysinfo = utsname()
        uname(&sysinfo) // ignore return value
        return String(bytes: Data(bytes: &sysinfo.machine, count: Int(_SYS_NAMELEN)), encoding: .ascii)!.trimmingCharacters(in: .controlCharacters)
    }
}

